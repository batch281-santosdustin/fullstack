let collection = [];

// Write the queue functions below.

function print() {
    // It will show the array
    // returns the collection array
    return collection;
}

function enqueue(element) {
    //In this function you are going to make an algo that will add an element to the array (last element)
    // Since the current collection size is not equal to the index, we can use the collection.length since it is equal to the last index + 1
    //collection[lastIndex + 1]
    collection[collection.length] = element;
    return collection;
   
}

function dequeue() {
    // In here you are going to remove the first element in the array
    // Example: collection = ["Jane", "Joe", "Bob"]
    for (let i = 0; i < collection.length - 1; i++) {
        // @ iteration 0; This will assign the element "Joe" to the index 0
        // @ iteration 1; Bob will be assigned to the index 1
        // @ iteration 2; null since nothing can be assigned
        collection[i] = collection [i + 1];
    }
    // since the last element is already null, it will be removed and will be just collection.length = 2
    collection.length--;
    return collection;
}

function front() {
    // you will get the first element
    return collection[0];
}

function size() {
     // Number of elements
    let count = 0;
    for (let i of collection) {
        count++;
    }
    return count;
}

function isEmpty() {
    //it will check whether the function is empty or not
    let count = 0;
    for (let i of collection) {
        count++;

        if(count > 0){
            return false;
        }

    }
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};