function countLetter(letter, sentence) {
    let count = 0;

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    // Checks if letter is more than 1
    if(letter.length !== 1) {
        return undefined;
    }

    // Checks how many occurrences in the the sentences
    // i represents the location of the letter being checked within the sentence
    // count is the counter for everytime the letter occurs in the sentence
    for (let i = 0; i < sentence.length; i++) {
        if(sentence[i] === letter) {
            count++;
        }
    }

    return count;
    
}


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

    // Sets the text input to lower case
    text = text.toLowerCase();

    // Checks every letter i lags from j since j will finish checking every letter before i moves to the next letter
    for (let i = 0; i < text.length; i++) {
        let count = 0;

        for (let j = 0; j < text.length; j++) {
            if (text[i] === text[j]) {
                count++;

                if(count > 1) {
                    return false;
                }
            }
        }
    }
    return true;
}

function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.

    // else if for 13 or 21 and 64 are separated since it will confuse the code
    if (age < 13) {
        return undefined;
    } else if(age >= 13 && age <= 21) {
        price *= 0.8;
    } else if (age >= 64) {
        price *= 0.8;
    }
    
    // Proper rounding since if not done by this, it will return accurate result
    price = Math.round(price * 100) / 100;
    return `${price}`;
}

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

    let categories = [];

    // of key is used to iterate every item
    for (let item of items) {
        // Checks if there are no stock anymore
        // Also checks if there is an already existing data of the same name in the categories array
        if(item.stocks === 0 && !categories.includes(item.category)){
            categories.push(item.category);
        }
    }

    return categories;
}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    
    let flyingVoters = [];

    // iterates on every voters of candidateA
    for (let voter of candidateA) {
        // checks if the voter of candidateA is existing in the voters of candidateB
        if(candidateB.includes(voter)){
            flyingVoters.push(voter);
        }
    }

    return flyingVoters;
    
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};