// [SECTION] Declaration
const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');

// [SECTION] Functions
// Updates the Full Name bar by waiting out details coming from #txt-first-name and #txt-last-name
const updateFullName = () => {
  const fullName = txtFirstName.value + ' ' + txtLastName.value;
  spanFullName.innerHTML = fullName;
};

// [SECTION] Event listeners
txtFirstName.addEventListener('keyup', updateFullName);
txtLastName.addEventListener('keyup', updateFullName);

txtFirstName.addEventListener('keyup', (event) => {
  console.log(event.target);
  console.log(event.target.value);
});

txtLastName.addEventListener('keyup', (event) => {
  console.log(event.target);
  console.log(event.target.value);
});