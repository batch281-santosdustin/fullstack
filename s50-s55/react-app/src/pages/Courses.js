import coursesData from '../data/coursesData';
import { Fragment, useState, useEffect } from 'react';
import CourseCard from '../components/CourseCard';

export default function Courses() {
	// Check if mock data is captured
	// console.log(coursesData);

	// "Map" method loops through the individual course objects in our array and returns a component for each course
	// Multiple components created through the map method must have a unique key - this will help React.js identify which components/ elements have been changed
	// For everytime the map method loops through the data, it creates a "CourseCard" component and then passes the current element in our courseData array using the courseProp
/*	const courses = coursesData.map(course => {
		return (
			<CourseCard key={course.id} courseProp={course} />
		)
	})
*/

	const [courses, setCourses] = useState([]);

	//we are goint to add an useEffect here so that in every time that we refresh our application it will fetch the updated content of our courses.
	// Fetches all ACTIVE courses
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/`)
			.then(response => response.json())
			.then(data => {
				// console.log(data);
				setCourses(data.map(course => {
					// console.log(course);
					return (
						<CourseCard key={course._id} courseProp={course} />
					)
				}))
			})
	}, [])

	// The course in the CourseCard component is called a prop which is shorthand for "property" since components are considered as objects in ReactJS
	// The curly braces ({}) are used for props to signify that we are providing information using JavaScript expressions rather than hard coded values which use double quotes ""
	// We can pass information from one component to another using props. This is referred to as props drilling.
	return (
		<Fragment>
			{courses}
			{/*Unnecessary to use anymore due to courses above*/}
			{/*<CourseCard  courseProp={coursesData[0]}/>*/}
		</Fragment>
	)
}