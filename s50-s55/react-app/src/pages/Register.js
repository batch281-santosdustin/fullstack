// import Button from 'react-bootstrap/Button';
// import Form from 'react-bootstrap/Form';

import {Button, Form, Row, Col} from 'react-bootstrap';
//we need to import the useState from the react
import {useState, useEffect, useContext} from 'react';

import {useNavigate} from 'react-router-dom';
import UserContext from '../UserContext';
import Error from './Error';

import Swal2 from 'sweetalert2';

export default function Register(){
	//State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	// const [password2, setPassword2] = useState('');

	const { user, setUser } = useContext(UserContext);

	const navigate = useNavigate();

	const [isPassed, setIsPassed] = useState(true);

	const [isFirstNamePassed, setFirstNameIsPassed] = useState(true);
	const [isLastNamePassed, setLastNameIsPassed] = useState(true);
	const [isMobileNoPassed, setMobileNoIsPassed] = useState(true);

	const [isDisabled, setIsDisabled] = useState(true);

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');

	//we are going to add/create a state that will declare whether the password1 and password 2 is equal
	const [isPasswordMatch, setIsPasswordMatch] = useState(true); 
 
 	//when the email changes it will have a side effect that will console its value
	useEffect(()=>{
		if(email.length > 15){
			setIsPassed(false);
		}else{
			setIsPassed(true);
		}
	}, [email]);

	useEffect(()=>{
		if(firstName.length > 11){
			setFirstNameIsPassed(false);
		}else{
			setFirstNameIsPassed(true);
		}
	}, [firstName]);

	useEffect(()=>{
		if(lastName.length > 11){
			setLastNameIsPassed(false);
		}else{
			setLastNameIsPassed(true);
		}
	}, [lastName]);

	useEffect(()=>{
		if(mobileNo.length > 11){
			setMobileNoIsPassed(false);
		}else{
			setMobileNoIsPassed(true);
		}
	}, [mobileNo]);

	//this useEffect will disable or enable our sign up button
	useEffect(()=> {
		//we are going to add if statement and all the condition that we mention should be satisfied before we enable the sign up button.
		if(firstName !== '' && lastName !== '' && email !== '' && password !== '' && email.length <= 15){
			setIsDisabled(false);
		}else{
			setIsDisabled(true);
		}
	}, [firstName, lastName, email, mobileNo, password]);

	//function to simulate user registration
	function registerUser(event) {
		//prevent page reloading
		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({ email }),
		})
			.then(response => response.json())
			.then(data => {
				if (data === true) {
					Swal2.fire({
						title: "Registration unsuccessful",
						icon: 'error',
						text: "Email already exists"
					})
				} else {
					fetch(`${process.env.REACT_APP_API_URL}/users/register`,{
						method: 'POST',
						headers: {
							'Content-Type' : 'application/json'
						},
						body: JSON.stringify({
							firstName: firstName,
							lastName: lastName,
							mobileNo: mobileNo,
							email: email,
							password: password
						})
					})
						.then(response => response.json())
						.then(data => {
							// console.log(data);
							if(data === false){
								Swal2.fire({
									title: "Registration unsuccessful",
									icon: 'error',
									text: "Check the details provided and try again"
								})
							} else {
								navigate('/login');
							}
						})
				}
			})

		

		// alert('Thank you for registering!');

		// navigate('/login');
		
	}

	//useEffect to validate whether the password1 is equal to password2
	// useEffect(() => {
	// 	if(password1 !== password2){
	// 		setIsPasswordMatch(false);
	// 	}else{
	// 		setIsPasswordMatch(true);
	// 	}

	// }, [password1, password2]);

	return(
		user.id === null || user.id === undefined
		?
		<Row>
			<Col className = "col-6 mx-auto">
				<h1 className = "text-center">Register</h1>
				<Form onSubmit ={event => registerUser(event)}>
						<Form.Group className="mb-3" controlId="formBasicfirstName">
						  <Form.Label>First name</Form.Label>
						  <Form.Control 
						  	type="firstName" 
						  	placeholder="Enter first name" 
						  	value = {firstName}
						  	onChange = {event => setFirstName(event.target.value)}
						  	/>
						  <Form.Text className="text-danger" hidden = {isFirstNamePassed}>
						    The email should not exceed 11 characters!
						  </Form.Text>
						</Form.Group>

						<Form.Group className="mb-3" controlId="formBasicLastName">
						  <Form.Label>Last name</Form.Label>
						  <Form.Control 
						  	type="lastName" 
						  	placeholder="Enter last name" 
						  	value = {lastName}
						  	onChange = {event => setLastName(event.target.value)}
						  	/>
						  <Form.Text className="text-danger" hidden = {isLastNamePassed}>
						    The email should not exceed 11 characters!
						  </Form.Text>
						</Form.Group>

						<Form.Group className="mb-3" controlId="formBasicMobileNo">
						  <Form.Label>Mobile Number</Form.Label>
						  <Form.Control 
						  	type="mobileNo" 
						  	placeholder="Enter mobile number" 
						  	value = {mobileNo}
						  	onChange = {event => setMobileNo(event.target.value)}
						  	/>
						  <Form.Text className="text-danger" hidden = {isMobileNoPassed}>
						    The email should not exceed 11 characters!
						  </Form.Text>
						</Form.Group>

				      <Form.Group className="mb-3" controlId="formBasicEmail">
				        <Form.Label>Email address</Form.Label>
				        <Form.Control 
				        	type="email" 
				        	placeholder="Enter email" 
				        	value = {email}
				        	onChange = {event => setEmail(event.target.value)}
				        	/>
				        <Form.Text className="text-danger" hidden = {isPassed}>
				          The email should not exceed 15 characters!
				        </Form.Text>
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="formBasicPassword">
				        <Form.Label>Password</Form.Label>
				        <Form.Control 
				        	type="password" 
				        	placeholder="Password" 
				        	value = {password}
				        	onChange = {event => setPassword(event.target.value)}
				        	/>
				      </Form.Group>

{/*				      <Form.Group className="mb-3" controlId="formBasicPassword2">
				        <Form.Label>Confirm Password</Form.Label>
				        <Form.Control 
				        	type="password" 
				        	placeholder="Retype your nominated password" 
				        	value = {password2}
				        	onChange = {event => setPassword2(event.target.value)}
				        	/>

				        <Form.Text className="text-danger" hidden = {isPasswordMatch}>
				          The passwords does not match!
				        </Form.Text>
				      </Form.Group>*/}

				      

				      <Button variant="primary" type="submit" disabled = {isDisabled}>
				        Sign up
				      </Button>
				</Form>
			</Col>
		</Row>
		:
		<Error/>
		)
}